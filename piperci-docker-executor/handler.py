import docker
import mimetypes
import hashlib
import tempfile
import os
import time

from flask import g
from piperci.gman import client as gman_client
from piperci.artman import artman_client
from piperci.storeman.client import storage_client
from piperci.sri import generate_sri

from .util import unzip_files, read_secrets, gman_activate
from .config import Config

gman_url = Config["gman"]["url"]
storage_url = Config["storage"]["url"]
function_name = Config["name"]
function_executor = f"{Config['name']}_executor"
executor_url = Config["executor_url"]


@gman_activate(status="received")
def handle(request):
    """
    Noop executor function definition. This handler function controls
    all functionality
    :param request: The request object from Flask
    This object is required to have the following JSON parameters:
    * run_id: The run_id of the task
    * thread_id: The thread_id, from gman, that this execution is forked from.
    * project: The project name of the run
    * configs: A list containing the configuration dictionaries for the run.
    * stage: The stage that is being run.
    * artifacts: A list of dictionaries containing information on the artifacts
    required for this run
    :param request:
    :return:
    """
    run_id = request.get_json().get("run_id")
    stage = request.get_json()["stage"]
    configs = request.get_json()["configs"]
    artifacts = request.get_json()["artifacts"]
    project = request.get_json()["project"]

    docker_file = next(config.get('docker-file') for config in configs)
    build_cmds = next(config.get('build-cmd') for config in configs)
    docker_tag = next(
        config.get("docker-tag", hashlib.shake_256(run_id.encode('utf-8')).hexdigest(8)) for config in configs)
    extra_artifacts = next(config.get("extra-artifacts") for config in configs)
    docker_image = next(config.get("docker-image") for config in configs)
    run_cmd = next(config.get("run-cmd") for config in configs)

    access_key = read_secrets().get("access_key")
    secret_key = read_secrets().get("secret_key")

    minio_client = storage_client(
        "minio", hostname=storage_url, access_key=access_key, secret_key=secret_key
    )

    with tempfile.TemporaryDirectory() as temp_directory:
        for art_name, art_data in artifacts.items():
            extract_artifact(minio_client, temp_directory, art_name, art_data)
        if extra_artifacts:
            for extra_art_name, extra_art_data in extra_artifacts.items():
                extract_artifact(minio_client, temp_directory, extra_art_name, extra_art_data)

        if docker_file:
            extract_artifact(minio_client, temp_directory, 'Dockerfile', {"artifact_uri": docker_file})
            log_file = f"{temp_directory}/{docker_tag}_build.log"
            project_directory = f"{temp_directory}/{project}.zip"
            dockerfile = f"{temp_directory}/Dockerfile/Dockerfile"
            docker_client = docker.APIClient(base_url="tcp://172.17.0.1:2376")
            build_args = {
                'path': project_directory,
                'dockerfile': dockerfile,
                'tag': docker_tag,
            }
            if build_cmds:
                build_args.update(build_cmds)
            os.chdir(project_directory)
            logs = docker_client.build(**build_args)
            errors = False
            with open(log_file, "wb+") as f:
                for line in logs:
                    f.write(line)
                    if "errorDetail" in line.decode():
                        errors = line
            minio_client.upload_file(
                run_id,
                f"artifacts/logs/{stage}/{os.path.basename(log_file)}",
                log_file,
            )
            if errors:
                raise ValueError(f"There was an error building the dockerfile. {errors}")

            if run_cmd:
                log_file = f"{temp_directory}/{docker_tag}.log"
                docker_client = docker.DockerClient(base_url="tcp://172.17.0.1:2376")
                logs = docker_client.containers.run(docker_tag, run_cmd)
                with open(log_file, "wb") as f:
                    f.write(logs)
                minio_client.upload_file(
                    run_id,
                    f"artifacts/logs/{stage}/{os.path.basename(log_file)}",
                    log_file,
                )

        if docker_image and run_cmd:
            log_file = f"{temp_directory}/{docker_image}.log"
            run_docker_cmd(docker_image, run_cmd, log_file)
            minio_client.upload_file(run_id, f"artifacts/logs/{stage}/{os.path.basename(log_file)}", log_file)


def extract_artifact(minio_client, temp_dir, art_name, art_data):
    art_path = os.path.join(temp_dir, art_name)
    os.mkdir(art_path)
    temp_art_location = os.path.join(temp_dir, f"{time.time()}-{art_name}")
    minio_client.download_file(
        art_data["artifact_uri"], temp_art_location
    )
    if mimetypes.guess_type(temp_art_location)[0] == 'application/zip':
        unzip_files(temp_art_location, art_path)
    else:
        os.rename(temp_art_location, os.path.join(art_path, art_name))
